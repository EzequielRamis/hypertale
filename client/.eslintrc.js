module.exports = {
  root: true,
  env: {
    browser: true,
    node: true,
  },
  parserOptions: {
    parser: 'babel-eslint',
  },
  extends: [
    '@nuxtjs',
    'plugin:nuxt/recommended',
    //'plugin:prettier/recommended',
    'prettier',
    'prettier/vue',
    'airbnb-base',
  ],
  plugins: ['prettier'],
  // add your custom rules here
  rules: {
    'nuxt/no-cjs-in-config': 'off',
    'max-len': ['error', { code: 300 }],
    'no-param-reassign': [2, { props: false }],
    'arrow-parens': 0,
  },
};
