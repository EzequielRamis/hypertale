module.exports = {
  mode: 'universal',
  /*
   ** Headers of the page
   */
  head: {
    title: process.env.npm_package_name || '',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      {
        hid: 'description',
        name: 'description',
        // eslint-disable-next-line prettier/prettier
        content: process.env.npm_package_description || '',
      },
    ],
    link: [{ rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }],
  },
  /*
   ** Customize the progress-bar color
   */
  loading: { color: '#e70074', continuous: true },
  /*
   ** Global CSS
   */
  css: [
    '@/assets/scss/settings/_settings.colors.scss',
    '@/assets/scss/settings/_settings.fonts.scss',
    '@/assets/scss/tools/_tools.mixins.scss',
    '@/assets/scss/tools/_tools.functions.scss',
    '@/assets/scss/generic/_generic.reset.scss',
    '@/assets/scss/elements/_elements.text.scss',
    '@/assets/scss/elements/_elements.body.scss',
    '@/assets/scss/elements/_elements.input.scss',
    '@/assets/scss/objects/_objects.scss',
    '@/assets/scss/objects/_objects.buttons.scss',
    '@/assets/scss/objects/_objects.colors.scss',
    '@/assets/scss/objects/_objects.text.scss',
    '@/assets/scss/objects/_objects.flex.scss',
    '@/assets/scss/objects/_objects.grid.scss',
    '@/assets/scss/objects/_objects.margin.scss',
    '@/assets/scss/objects/_objects.margin-top.scss',
    '@/assets/scss/objects/_objects.margin-right.scss',
    '@/assets/scss/objects/_objects.margin-bottom.scss',
    '@/assets/scss/objects/_objects.margin-left.scss',
    '@/assets/scss/objects/_objects.padding.scss',
    '@/assets/scss/objects/_objects.padding-top.scss',
    '@/assets/scss/objects/_objects.padding-right.scss',
    '@/assets/scss/objects/_objects.padding-bottom.scss',
    '@/assets/scss/objects/_objects.padding-left.scss',
  ],
  /*
   ** Plugins to load before mounting the App
   */
  plugins: [],
  /*
   ** Nuxt.js modules
   */
  modules: [
    '@nuxtjs/pwa',
    '@nuxtjs/eslint-module',
    '@nuxtjs/style-resources',
    '@nuxtjs/axios',
    '@nuxtjs/auth',
  ],
  /*
   ** Build configuration
   */
  build: {
    /*
     ** You can extend webpack config here
     */
    extend(config, ctx) {
      // Run ESLint on save
      if (ctx.isDev && ctx.isClient) {
        config.module.rules.push({
          enforce: 'pre',
          test: /\.(js|vue)$/,
          loader: 'eslint-loader',
          exclude: /(node_modules)/,
        });
      }
    },
  },

  server: {
    host: '0.0.0.0',
    port: 40005, // default: 3000
  },

  styleResources: {
    scss: [
      'assets/scss/settings/_settings.colors.scss',
      'assets/scss/settings/_settings.fonts.scss',
      'assets/scss/tools/_tools.mixins.scss',
      'assets/scss/tools/_tools.functions.scss',
      'assets/scss/generic/_generic.reset.scss',
      'assets/scss/elements/_elements.text.scss',
      'assets/scss/elements/_elements.body.scss',
      'assets/scss/elements/_elements.input.scss',
      'assets/scss/objects/_objects.scss',
      'assets/scss/objects/_objects.buttons.scss',
      'assets/scss/objects/_objects.colors.scss',
      'assets/scss/objects/_objects.text.scss',
      'assets/scss/objects/_objects.flex.scss',
      'assets/scss/objects/_objects.grid.scss',
      'assets/scss/objects/_objects.margin.scss',
      'assets/scss/objects/_objects.margin-top.scss',
      'assets/scss/objects/_objects.margin-right.scss',
      'assets/scss/objects/_objects.margin-bottom.scss',
      'assets/scss/objects/_objects.margin-left.scss',
      'assets/scss/objects/_objects.padding.scss',
      'assets/scss/objects/_objects.padding-top.scss',
      'assets/scss/objects/_objects.padding-right.scss',
      'assets/scss/objects/_objects.padding-bottom.scss',
      'assets/scss/objects/_objects.padding-left.scss',
    ],
  },

  env: {
    apiUrl:
      process.env.API_URL ||
      'http://ec2-3-227-15-61.compute-1.amazonaws.com:40004',
  },

  auth: {
    plugins: ['~/plugins/auth.js'],
    cookie: true,
    resetOnError: true,
    redirect: {
      login: '/signin',
      logout: '/',
      callback: '/signin',
      home: '/',
    },
    strategies: {
      strapi: {
        _scheme: '~/schemes/strapi.js',
        endpoints: {
          login: {
            url: '/auth/local',
            method: 'post',
            propertyName: 'jwt',
          },
          user: {
            url: '/users/me',
            method: 'get',
          },
        },
      },
    },
  },
  axios: {
    baseURL: 'http://ec2-3-227-15-61.compute-1.amazonaws.com:40004',
  },
};
